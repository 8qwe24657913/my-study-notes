#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

with open('./石头记.txt', mode='r', encoding='UTF-8') as readFrom:
    with open('./红楼梦.txt', mode='w', encoding='UTF-8') as writeTo:
        txt = readFrom.read()
        txt = re.sub(r'(?<!br/>) ', r'\n ', txt) # 不知道哪里来的 "br/> " 不应该被处理……
        txt = re.sub(r'\n*(?:(?<=&#)(_分节阅读_\d+)(?=39;)|(?<=&#3)(_分节阅读_\d+)(?=9;)|(_分节阅读_\d+))\n*', r'\n\n\1\2\3\n\n', txt).strip() # &#39; 会被拆开可海星
        txt = txt + '\n 欢迎访问:http://www.txt.cn' # 甚至会多出字来……
        writeTo.write(txt)
        print(txt.count('之'))
