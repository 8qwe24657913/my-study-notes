#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

if len(sys.argv) is not 4:
    raise ValueError('Expect 3 arguments, got {0} instead.'.format(len(sys.argv) - 1))

_, num1, operator, num2 = sys.argv

nums = tuple(map(int, (num1, num2)))
for i in nums:
    if i < -16384 or i > 16383:
        raise ValueError('Operands are expected to be in the range [-16384, 16383], got {0} instead.'.format(i))

operators = {
    '+': lambda a, b: a + b,
    '-': lambda a, b: a - b,
    '*': lambda a, b: a * b,
    '/': lambda a, b: a // b, # 地板除
}
func = operators.get(operator)
if (func is None):
    raise ValueError('Illegal operator: {0}'.format(operator))

print(func(*nums))
