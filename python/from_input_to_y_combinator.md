# 一个表达式引起的血案 - 从 py 2 的 input() 到 Y 组合子
众所周知，`python 2` 中的 `input(xxx)` 相当于 `python 3` 中的 `eval(raw_input(xxx))`，因此会带来意料之外的安全隐患，例如：

```python
print input('Please input: ')
```

输入的表达式会被执行，比如输入`1+1`则会得到`2`

防治很简单，如 `ast.literal_eval()`，这里不赘述了，因为本文的重点是讨论我们能在一个表达式里作出什么妖来，类似于知乎问题 [一行代码可以做什么？](https://www.zhihu.com/question/285586045)，但要求更加严格一点

单纯写 `1+1` 其实很没有意思，最多也就能再写几个函数调用，用一用三目运算符 `x if y else z`，如果我们想写一个循环呢？

```python
for i in range(0, 3): print '丁寧'
# 报错：
"""
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 1
    for i in range(0, 3): print '丁寧'
      ^
SyntaxError: invalid syntax
"""
```

其原因在于 `for` 和 `print` 都是**语句(statement)**，不是**表达式(expression)**，不能 `eval`，只能 `exec`

但 py2 中 `exec` 也是一个语句（py3 中 `exec` 和 `print` 是函数），仿佛开箱子的钥匙被锁在了箱子里

能不能再给力一点啊，老师？

当然可以，别忘了**列表生成式 / 生成器**：

```python
'\n'.join(['丁寧' for i in range(0, 3)])
```

但有时我们需要在循环中使用上一次循环的结果，能不能再给力一点啊，老师？

上`reduce()`函数和**lambda 表达式**：

```python
reduce(lambda x, y: x + y, range(1, 101)) # 5050
```

借助 `range()` 函数，我们可以很简单地写出次数已知的循环

那要是循环次数未知呢？能不能再给力一点啊，老师？

我们可以使用**循环变递归**的技巧，改用递归完成这个任务，首先我们构造一个递归函数：

```python
recurse = lambda n: '抱着' + recurse(n - 1) + '的我' if n else '我的小鲤鱼'
# 演示
print recurse(3) # 抱着抱着抱着我的小鲤鱼的我的我的我
# 但你要看到我们使用了一个赋值语句，不是一个表达式，所以 eval 会报错
"""
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 1
    recurse = lambda n: '抱着' + recurse(n - 1) + '的我' if n else '我的小鲤鱼'
            ^
SyntaxError: invalid syntax
"""
```

能不能再给力一点啊，老师？

当然，我们可以使用**函数式**的思想，搞一个**高阶函数** `f` ，这样就能绕过为变量 `recurse` 赋值的操作了

```python
f = lambda recurse: lambda n: '抱着' + recurse(n - 1) + '的我' if n else '我的小鲤鱼'
```

我们发现 `recurse = f(recurse)`，再展开，`recurse = f(f(recurse))`……我们发现我们永远展开不完它

我们需要一个函数`g`，使`g() = recurse`

```python
g = lambda: recurse
# 既然 recurse = f(recurse)，g() = recurse，两式消掉 recurse，recurse = g() = f(g())
g = lambda: f(g())
```

我们还是依赖于一个变量 `g`，还是用高阶函数的技巧，给 `g()` 传入一个参数 `g`

```python
g = lambda g: f(g(g))
# 此时就可以去掉前面的 g = 了，为此我们需要加一个辅助函数
recurse = (lambda g: g(g))(lambda g: f(g(g)))
```

但如果你试过的话，直接这么写会不停地执行 `g(g)` 直到爆栈。能不能再给力一点啊，老师？

我们需要**[惰性求值](https://zh.wikipedia.org/wiki/%E6%83%B0%E6%80%A7%E6%B1%82%E5%80%BC)**，这里我们只需将 `g(g)` 包裹一层，变成 `lambda *args: g(g)(*args)` 

注：正统的 [lambda 演算](https://zh.wikipedia.org/wiki/%CE%9B%E6%BC%94%E7%AE%97)是不能传多个参数的，这里用 `*args` 只是为了方便而已

```python
recurse = (lambda g: g(g))(lambda g: f(lambda *args: g(g)(*args)))
```

对于 `f` 也一样，使用高阶函数传入即可

```python
recurse = (lambda f: (lambda g: g(g))(lambda g: f(lambda *args: g(g)(*args))))(f)
```

大功告成！让我们提取出其中与 `recurse` 函数无关的代码，起名 `Y`

```python
Y = lambda f: (lambda g: g(g))(lambda g: f(lambda *args: g(g)(*args)))
f = lambda recurse: lambda n: '抱着' + recurse(n - 1) + '的我' if n else '我的小鲤鱼'
recurse = Y(f)
```

之所以起名为`Y`，是因为它有一个高大上的名字：**[Y 组合子(Y Combinator)](https://zh.wikipedia.org/wiki/%E4%B8%8D%E5%8A%A8%E7%82%B9%E7%BB%84%E5%90%88%E5%AD%90)**

这个函数可以用在不同的递归函数上，比如我们写个递归的 `gcd` 函数作为测试

```python
getGcd = lambda gcd: lambda m, n: gcd(n, m % n) if m % n else n
```

让我们试一试（为了排版不过长，请自行替换 `Y` 与 `getGcd` 函数）：

```python
(Y)(getGcd)(114, 514) # 2
```

回到我们最初的问题：_能在一个表达式里作出什么妖来？_ 

答：_可以作一切你能想到的妖_



后记：

`input(xxx)` 的安全漏洞本来是去年玩的东西，最近一想：不能循环只能递归，却又不许赋值，这不正好是 Y 组合子登场的时候吗。于是写了这篇文章，大家共同学习，如有错漏请多指正。

附录一、你绝对不会想要读下去的扩展阅读：[Y 组合子(Y Combinator)](https://zh.wikipedia.org/wiki/%E4%B8%8D%E5%8A%A8%E7%82%B9%E7%BB%84%E5%90%88%E5%AD%90)

附录二、玩梗的解释

1. [丁寧丁寧丁寧](https://zh.moegirl.org/%E6%96%B0%E5%AE%9D%E5%B2%9B)
2. 能不能再给力一点啊，老师？  -  未找到解释，我是在 [知乎某回答](https://www.zhihu.com/question/20509121/answer/15329340) 看到的，英文原文版并非如此
3. [抱着抱着抱着我的小鲤鱼的我的我的我](https://www.zhihu.com/question/40752549/answer/100694228)
4. [114514](https://zh.moegirl.org/%E9%87%8E%E5%85%BD%E5%85%88%E8%BE%88%E5%A5%B3%E5%AD%90%E8%AF%B4)