from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect

def index(request):
    return render(request, 'login.html', {})

def login(request):
    if request.method != 'POST':
        return HttpResponse('Invalid method!')

    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return HttpResponseRedirect('/login/loggedin/')
    else:
        return HttpResponse('Username or password invalid!')

@login_required(login_url='/login/')
def loggedin(request):
    return render(request, 'loggedin.html', {'username': request.user.username})
